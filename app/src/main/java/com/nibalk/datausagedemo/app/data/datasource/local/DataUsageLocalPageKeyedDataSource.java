package com.nibalk.datausagedemo.app.data.datasource.local;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nibalk.datausagedemo.app.data.database.YearlyUsageDao;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;

import java.util.List;

public class DataUsageLocalPageKeyedDataSource extends PageKeyedDataSource<Integer, YearlyUsage> {

    private final String TAG = "DUApp-" + DataUsageLocalPageKeyedDataSource.this.getClass().getSimpleName();

    private YearlyUsageDao yearlyUsageDao;

    public DataUsageLocalPageKeyedDataSource(YearlyUsageDao yearlyUsageDao) {
        this.yearlyUsageDao = yearlyUsageDao;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, YearlyUsage> callback) {
        Log.i(TAG, "Loading Initial Rang, Count " + params.requestedLoadSize);
        List<YearlyUsage> yearlyUsagesList = yearlyUsageDao.getYearlyUsage();
        if(yearlyUsagesList.size() > 0) {
            callback.onResult(yearlyUsagesList, 0, 1);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, YearlyUsage> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, YearlyUsage> callback) {

    }

}
