package com.nibalk.datausagedemo.app.data.service;

import com.nibalk.datausagedemo.app.data.model.DataUsage;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.nibalk.datausagedemo.app.utils.Constants.AppUrls.FETCH_DATA_USAGE;

public interface DataUsageService {

    // @FormUrlEncoded
    @GET(FETCH_DATA_USAGE)
    Call<DataUsage> fetDataUsageInfo(
            @Query("offset") int source,
            @Query("limit") int limit,
            @Query("resource_id") String apiKey);

}
