package com.nibalk.datausagedemo.app.view.adapter;

import android.arch.paging.PagedListAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nibalk.datausagedemo.R;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;
import com.nibalk.datausagedemo.app.view.utils.YearlyUsageListItemClickListener;
import com.nibalk.datausagedemo.databinding.LayoutCardviewBinding;

public class YearlyUsageListAdapter extends PagedListAdapter<YearlyUsage, YearlyUsageListAdapter.YearlyUsageViewHolder> {

    private YearlyUsageListItemClickListener listener;

    public YearlyUsageListAdapter(YearlyUsageListItemClickListener listener) {
        super(YearlyUsage.DIFF_CALLBACK);

        this.listener = listener;
    }

    @NonNull
    @Override
    public YearlyUsageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutCardviewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.layout_cardview, parent, false);

        return new YearlyUsageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final YearlyUsageViewHolder holder, int position) {
        holder.binding.setModel(getItem(position));
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public YearlyUsage getAdapterItem(int position) {
        return getItem(position);
    }

    class YearlyUsageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        LayoutCardviewBinding binding;

        YearlyUsageViewHolder(LayoutCardviewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

            itemView.dataUsageCardView.setOnClickListener(this);
            binding.ivStar.setOnClickListener(this);
            binding.ivStar.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            boolean isImageClicked = v.getId() == binding.ivStar.getId();
            listener.onClicked(getAdapterPosition(), isImageClicked);
        }

        @Override
        public boolean onLongClick(View v) {
            listener.onLongClicked(getAdapterPosition());
            return true;
        }
    }
}