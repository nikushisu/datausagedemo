package com.nibalk.datausagedemo.app.di.module;

import android.content.Context;

import com.nibalk.datausagedemo.app.data.repository.DataUsageRepository;
import com.nibalk.datausagedemo.app.data.repository.DataUsageRepositoryImpl;
import com.nibalk.datausagedemo.app.data.service.DataUsageService;
import com.nibalk.framework.network.service.WebServiceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class DataUsageModule {

    @Provides
    @Singleton
    DataUsageRepository provideDataUsageRepository(Context context,
                                                   DataUsageService dataUsageService,
                                                   WebServiceManager webServiceManager) {
        return new DataUsageRepositoryImpl(context, dataUsageService, webServiceManager);
    }

    @Provides
    @Singleton
    DataUsageService provideDataUsageService(Retrofit retrofit) {
        return retrofit.create(DataUsageService.class);
    }
}
