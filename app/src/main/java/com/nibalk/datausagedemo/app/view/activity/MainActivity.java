package com.nibalk.datausagedemo.app.view.activity;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.nibalk.datausagedemo.R;
import com.nibalk.datausagedemo.app.view.adapter.YearlyUsageListAdapter;
import com.nibalk.datausagedemo.app.view.utils.YearlyUsageListItemClickListener;
import com.nibalk.datausagedemo.app.viewmodel.MainActivityViewModel;
import com.nibalk.datausagedemo.databinding.ActivityMainBinding;
import com.nibalk.framework.base.view.activity.BaseActivity;

import javax.inject.Inject;

import timber.log.Timber;

public class MainActivity extends BaseActivity<MainActivityViewModel, ActivityMainBinding> {

    @Inject
    ConnectivityManager connectivityManager;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private ActivityMainBinding binding;
    private MainActivityViewModel viewModel;

    private YearlyUsageListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.d("onCreate");
        setContentView(R.layout.activity_main);

        super.onCreate(savedInstanceState);

        /**
         * When device is online - app will fetch data from server and store it in the SQLite DB
         * When device is offline - app will fetch data from SQLite DB
         */
        displayDataUsage();
    }

    @Override
    protected MainActivityViewModel setupViewModel() {
        Timber.d("setupViewModel");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel.class);
        return viewModel;
    }

    @Override
    protected ActivityMainBinding setupDataBinding() {
        Timber.d("setupDataBinding");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        return binding;
    }

    private void displayDataUsage() {

        binding.list.setLayoutManager(new LinearLayoutManager(this));

        adapter = new YearlyUsageListAdapter(listItemClickListener);
        binding.list.setAdapter(adapter);

        viewModel.getYearlyUsageData().observe(this, yearlyUsages -> {
            adapter.submitList(yearlyUsages);
        });

        viewModel.getProgressStatus().observe(this, status -> {
            switch (status) {
                case RUNNING:
                    binding.progress.setVisibility(View.VISIBLE);
                    break;
                case SUCCESS:
                    binding.progress.setVisibility(View.GONE);
                    break;
                case ERROR:
                    binding.progress.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content),
                            "Device is offline. Can not connect to server.", Snackbar.LENGTH_SHORT)
                            .show();
                    break;
                case FAILED:
                    binding.progress.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content),
                            "Server connection failed", Snackbar.LENGTH_SHORT)
                            .show();
                    break;
            }
        });

    }

    private YearlyUsageListItemClickListener listItemClickListener = new YearlyUsageListItemClickListener() {
        @Override
        public void onClicked(int position, boolean isImageClicked) {
            if (isImageClicked) {
                //YearlyUsage usage = adapter.getAdapterItem(position);
                Toast.makeText(MainActivity.this, "Clicked image at row " + (position + 1),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Clicked list row at " + (position + 1),
                        Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onLongClicked(int position) {
            Toast.makeText(MainActivity.this, "Long clicked image at " + position,
                    Toast.LENGTH_SHORT).show();
        }
    };
}
