package com.nibalk.datausagedemo.app.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.paging.PagedList;
import android.content.Context;
import android.support.annotation.Nullable;

import com.nibalk.datausagedemo.app.data.database.YearlyUsageDB;
import com.nibalk.datausagedemo.app.data.datasource.local.DataUsageLocalDataSourceFactory;
import com.nibalk.datausagedemo.app.data.datasource.remote.DataUsageRemoteDataSourceFactory;
import com.nibalk.datausagedemo.app.data.model.ProgressStatus;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;
import com.nibalk.datausagedemo.app.data.repository.local.DataUsageLocalRepository;
import com.nibalk.datausagedemo.app.data.repository.remote.DataUsageRemoteRepository;
import com.nibalk.datausagedemo.app.data.service.DataUsageService;
import com.nibalk.framework.network.service.WebServiceManager;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


public class DataUsageRepositoryImpl implements DataUsageRepository {
    
    private YearlyUsageDB database;
    private DataUsageRemoteRepository remoteRepository;
    private DataUsageLocalRepository localRepository;
    private DataUsageRemoteDataSourceFactory remoteDataSourceFactory;
    private DataUsageLocalDataSourceFactory localDataSourceFactory;

    private CompositeDisposable compositeDisposable;
    private MediatorLiveData liveDataMerger;

    private DataUsageService dataUsageService;
    private WebServiceManager webServiceManager;


    public DataUsageRepositoryImpl(Context context, DataUsageService dataUsageService,
                                   WebServiceManager webServiceManager) {

        this.database = YearlyUsageDB.getYearlyUsageDB(context.getApplicationContext());

        this.dataUsageService = dataUsageService;
        this.webServiceManager = webServiceManager;
    }

    @Override
    public void initFetching(CompositeDisposable disposable) {

        Timber.d("initFetching()");

        compositeDisposable = disposable;
        remoteDataSourceFactory = new DataUsageRemoteDataSourceFactory(dataUsageService,
                webServiceManager, compositeDisposable);
        localDataSourceFactory = new DataUsageLocalDataSourceFactory(
                database.getYearlyUsageDao());

        remoteRepository = new DataUsageRemoteRepository(remoteDataSourceFactory, boundaryCallback);
        localRepository = new DataUsageLocalRepository(localDataSourceFactory);
        liveDataMerger = new MediatorLiveData<>();

        liveDataMerger.addSource(remoteRepository.getYearlyUsagePagedList(), new Observer() {
            @Override
            public void onChanged(@Nullable Object value) {
                Timber.d("AddSource Remote - " + value.toString());
                liveDataMerger.setValue(value);
            }
        });

        getYearlyUsage();

    }

    @Override
    public LiveData<PagedList<YearlyUsage>> getYearlyUsageData(){
        return  liveDataMerger;
    }

    @Override
    public LiveData<ProgressStatus> getProgressStatus() {
        return remoteRepository.getLiveProgressStatus();
    }

    private void getYearlyUsage() {
        Timber.d("getYearlyUsage()");

        Disposable disposable = remoteDataSourceFactory.getYearlyDataUsage()
                .observeOn(Schedulers.io())
                .subscribe(yearlyUsage -> {
                    Timber.d("getYearlyUsage - response");

                    if (yearlyUsage != null) {
                        Timber.d("getYearlyUsage - response - success");
                        Timber.d("Storing Data == " + yearlyUsage.toString());

                        database.getYearlyUsageDao().insertYearlyUsage(yearlyUsage);
                    } else {
                        Timber.d("getYearlyUsage - response - fail");
                    }
                }, throwable -> {
                    Timber.d("getYearlyUsage - response - error");
                });
        compositeDisposable.add(disposable);
    }

    private PagedList.BoundaryCallback<YearlyUsage> boundaryCallback = new PagedList.BoundaryCallback<YearlyUsage>() {
        @Override
        public void onZeroItemsLoaded() {
            super.onZeroItemsLoaded();
            liveDataMerger.addSource(localRepository.getYearlyUsagePagedList(), value -> {
                Timber.d("AddSource Local -- " + value.toString());
                liveDataMerger.setValue(value);
                liveDataMerger.removeSource(localRepository.getYearlyUsagePagedList());
            });
        }
    };
}
