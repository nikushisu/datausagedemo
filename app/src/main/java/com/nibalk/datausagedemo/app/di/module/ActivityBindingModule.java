package com.nibalk.datausagedemo.app.di.module;

import com.nibalk.datausagedemo.app.di.MainActivityModule;
import com.nibalk.datausagedemo.app.view.activity.MainActivity;
import com.nibalk.framework.base.di.annotation.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindingMainActivity();
}
