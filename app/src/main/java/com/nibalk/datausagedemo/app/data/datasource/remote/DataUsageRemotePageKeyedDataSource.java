package com.nibalk.datausagedemo.app.data.datasource.remote;

import android.annotation.SuppressLint;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.os.Build;
import android.support.annotation.NonNull;

import com.nibalk.datausagedemo.app.data.model.DataUsage;
import com.nibalk.datausagedemo.app.data.model.ProgressStatus;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;
import com.nibalk.datausagedemo.app.data.service.DataUsageService;
import com.nibalk.framework.network.service.WebServiceManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import retrofit2.Call;
import timber.log.Timber;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.QUERY_PARAMS_LIMIT;
import static com.nibalk.datausagedemo.app.utils.Constants.Values.QUERY_PARAMS_OFFSET;
import static com.nibalk.datausagedemo.app.utils.Constants.Values.QUERY_PARAMS_RESOURCE_ID;
import static com.nibalk.datausagedemo.app.utils.Constants.Values.STARTING_OFFSET;

public class DataUsageRemotePageKeyedDataSource extends PageKeyedDataSource<Integer, YearlyUsage> {

    private DataUsageService dataUsageService;
    private WebServiceManager webServiceManager;
    private CompositeDisposable compositeDisposable;

    private MutableLiveData progressLiveStatus;
    private ReplaySubject<YearlyUsage> yearlyDataUsage;

    private ArrayList<YearlyUsage> emptyList = new ArrayList<>();

    public DataUsageRemotePageKeyedDataSource(DataUsageService dataUsageService,
                                             WebServiceManager webServiceManager,
                                             CompositeDisposable compositeDisposable) {
        this.dataUsageService = dataUsageService;
        this.webServiceManager = webServiceManager;
        this.compositeDisposable = compositeDisposable;

        this.progressLiveStatus = new MutableLiveData<>();
        this.yearlyDataUsage = ReplaySubject.create();

        setYearlyDataUsage(new ArrayList<>());
    }

    public MutableLiveData getProgressLiveStatus() {
        return progressLiveStatus;
    }

    public ReplaySubject<YearlyUsage> getYearlyDataUsage() {
        return yearlyDataUsage;
    }

    private Observable<DataUsage> fetchDataUsage(int offset) {
        Timber.d("fetchDataUsage()");

        Call<DataUsage> call = dataUsageService.fetDataUsageInfo(offset, QUERY_PARAMS_LIMIT, QUERY_PARAMS_RESOURCE_ID);

        return webServiceManager.execute(call)
                .map(dataUsage -> {
                    if (dataUsage != null) {
                        Timber.d(dataUsage.toString());
                    } else {
                        Timber.d("Fetched DataUsage content is null");
                    }
                    return dataUsage;
                });
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params,
                            @NonNull LoadInitialCallback<Integer, YearlyUsage> callback) {

        Timber.d("loadInitial");

        Disposable disposable = fetchDataUsage(STARTING_OFFSET)
                .doOnSubscribe(disposable1 -> progressLiveStatus.postValue(ProgressStatus.RUNNING))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataUsage -> {
                    Timber.d("loadInitial - response");

                    if (dataUsage != null) {
                        Timber.d("loadInitial - response - success");
                        progressLiveStatus.setValue(ProgressStatus.SUCCESS);

                        ArrayList<YearlyUsage> list = getYearlyList(
                                dataUsage.getResult().getRecords());
                        callback.onResult(list,null,
                                STARTING_OFFSET + QUERY_PARAMS_OFFSET);
                        setYearlyDataUsage(list);

                    } else {
                        Timber.d("loadInitial - response - fail");
                        progressLiveStatus.setValue(ProgressStatus.FAILED);

                        callback.onResult(emptyList,null,
                                STARTING_OFFSET + QUERY_PARAMS_OFFSET);
                        setYearlyDataUsage(emptyList);
                    }
                }, throwable -> {
                    Timber.d("loadInitial - error");
                    progressLiveStatus.setValue(ProgressStatus.ERROR);

                    callback.onResult(emptyList,null,
                            STARTING_OFFSET + QUERY_PARAMS_OFFSET);
                    setYearlyDataUsage(emptyList);
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params,
                           @NonNull LoadCallback<Integer, YearlyUsage> callback) {

    }

    @SuppressLint("CheckResult")
    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params,
                          @NonNull LoadCallback<Integer, YearlyUsage> callback) {

        Timber.d("loadAfter");

        Disposable disposable = fetchDataUsage(params.key)
                .doOnSubscribe(disposable1 -> progressLiveStatus.postValue(ProgressStatus.RUNNING))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataUsage -> {
                    Timber.d("loadAfter - response");

                    if (dataUsage != null) {
                        Timber.d("loadAfter - response - success");
                        progressLiveStatus.setValue(ProgressStatus.SUCCESS);

                        ArrayList<YearlyUsage> list = getYearlyList(
                                dataUsage.getResult().getRecords());
                        callback.onResult(list,params.key + QUERY_PARAMS_OFFSET);
                        setYearlyDataUsage(list);

                    } else {
                        Timber.d("loadAfter - response - fail");
                        progressLiveStatus.setValue(ProgressStatus.FAILED);

                        callback.onResult(emptyList,params.key + QUERY_PARAMS_OFFSET);
                        setYearlyDataUsage(emptyList);
                    }
                }, throwable -> {
                    Timber.d("loadAfter - response - error");
                    progressLiveStatus.setValue(ProgressStatus.ERROR);

                    callback.onResult(emptyList,params.key + QUERY_PARAMS_OFFSET);
                    setYearlyDataUsage(emptyList);
                });
        compositeDisposable.add(disposable);
    }

    private void setYearlyDataUsage(ArrayList<YearlyUsage> list) {
        ReplaySubject<YearlyUsage> yearlyUsageReplaySubject = yearlyDataUsage;
        for (YearlyUsage yearlyUsage : list) {
            yearlyUsageReplaySubject.onNext(yearlyUsage);
        }
        Timber.d("setYearlyDataUsage == " + yearlyDataUsage.toString());
    }

    private ArrayList<YearlyUsage> getYearlyList(List recordsList)  {
        ArrayList<DataUsage.Records> quarterlyList = new ArrayList(recordsList);
        ArrayList<YearlyUsage> yearlyList = new ArrayList();

        String separator = "\n";
        DecimalFormat df = new DecimalFormat("###.#####");
        StringBuilder sb = new StringBuilder();

        String currentYear = "";
        String currentQuarter = "";

        double previousQuarterVolume = 0.0;
        double currentYearTotal = 0.0;
        boolean isShowImage = false;

        if (quarterlyList != null && quarterlyList.size() > 0) {
            for(DataUsage.Records record : quarterlyList) {
                String[] output = record.getQuarter().split(Pattern.quote("-"));
                double volume = Double.parseDouble(record.getVolumeOfMobileData());

                currentYear = output[0];
                currentQuarter = output[1];

                sb.append(currentQuarter + ": " + record.getVolumeOfMobileData());
                sb.append(separator);

                currentYearTotal += volume;
                isShowImage = previousQuarterVolume > volume;
                previousQuarterVolume = volume;

                Timber.d("year = " + output[0] + " | quarter = " + output[1] + " | volume = " + volume +
                        " | total = " + currentYearTotal + " | formatted = " + df.format(currentYearTotal));
            }

            yearlyList.add(new YearlyUsage(currentYear,
                    sb.toString().substring(0, sb.length() - separator.length()),
                    df.format(currentYearTotal) + "", isShowImage));
        }

        return yearlyList;
    }
}
