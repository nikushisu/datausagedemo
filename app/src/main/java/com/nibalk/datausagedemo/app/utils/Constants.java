package com.nibalk.datausagedemo.app.utils;

public class Constants {

    public static class AppUrls {
        public static final String BASE_URL = "https://data.gov.sg/api/action/";
        public static final String FETCH_DATA_USAGE = "datastore_search";
    }

    public static class Values {
        public final static String QUERY_PARAMS_RESOURCE_ID = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f";
        public static final String DB_NAME = "DataUsageDB.db";
        public static final String DB_TABLE_NAME = "YearlyUsage";
        public static final int DB_VERSION = 1;
        public final static int QUERY_PARAMS_LIMIT = 4;
        public final static int QUERY_PARAMS_OFFSET = 4;
        public final static int STARTING_OFFSET = 14;
        public static final int NUMBERS_OF_THREADS = 3;
    }

    public static class Keys {
        public final static String IS_LOADED = "LOADED";
        public final static String IS_LOADING = "LOADING";
    }
}
