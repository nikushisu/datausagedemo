package com.nibalk.datausagedemo.app.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataUsage {


    @Expose
    @SerializedName("result")
    private Result result;
    @Expose
    @SerializedName("success")
    private boolean success;
    @Expose
    @SerializedName("help")
    private String help;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    @Override
    public String toString() {
        return "DataUsage{" +
                "result=" + result +
                ", success=" + success +
                ", help='" + help + '\'' +
                '}';
    }

    public static class Result {
        @Expose
        @SerializedName("total")
        private int total;
        @Expose
        @SerializedName("limit")
        private int limit;
        @Expose
        @SerializedName("offset")
        private int offset;
        @Expose
        @SerializedName("_links")
        private Links links;
        @Expose
        @SerializedName("records")
        private List<Records> records;
        @Expose
        @SerializedName("fields")
        private List<Fields> fields;
        @Expose
        @SerializedName("resource_id")
        private String resourceId;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public Links getLinks() {
            return links;
        }

        public void set_links(Links links) {
            this.links = links;
        }

        public List<Records> getRecords() {
            return records;
        }

        public void setRecords(List<Records> records) {
            this.records = records;
        }

        public List<Fields> getFields() {
            return fields;
        }

        public void setFields(List<Fields> fields) {
            this.fields = fields;
        }

        public String getResourceId() {
            return resourceId;
        }

        public void setResourceId(String resourceId) {
            this.resourceId = resourceId;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "total=" + total +
                    ", limit=" + limit +
                    ", offset=" + offset +
                    ", links=" + links +
                    ", records=" + records +
                    ", fields=" + fields +
                    ", resourceId='" + resourceId + '\'' +
                    '}';
        }
    }

    public static class Links {
        @Expose
        @SerializedName("next")
        private String next;
        @Expose
        @SerializedName("start")
        private String start;

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        @Override
        public String toString() {
            return "_links{" +
                    "next='" + next + '\'' +
                    ", start='" + start + '\'' +
                    '}';
        }
    }

    public static class Records {
        @Expose
        @SerializedName("_id")
        private int id;
        @Expose
        @SerializedName("quarter")
        private String quarter;
        @Expose
        @SerializedName("volume_of_mobile_data")
        private String volumeOfMobileData;

        public int getId() {
            return id;
        }

        public void set_id(int id) {
            this.id = id;
        }

        public String getQuarter() {
            return quarter;
        }

        public void setQuarter(String quarter) {
            this.quarter = quarter;
        }

        public String getVolumeOfMobileData() {
            return volumeOfMobileData;
        }

        public void setVolumeOfMobileData(String volumeOfMobileData) {
            this.volumeOfMobileData = volumeOfMobileData;
        }

        @Override
        public String toString() {
            return "Records{" +
                    "_id=" + id +
                    ", quarter='" + quarter + '\'' +
                    ", volume_of_mobile_data='" + volumeOfMobileData + '\'' +
                    '}';
        }
    }

    public static class Fields {
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("type")
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "Fields{" +
                    "id='" + id + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }
}
