package com.nibalk.datausagedemo.app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import com.nibalk.datausagedemo.app.data.model.ProgressStatus;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;
import com.nibalk.datausagedemo.app.data.repository.DataUsageRepository;
import com.nibalk.framework.base.viewmodel.BaseViewModel;

public class MainActivityViewModel extends BaseViewModel {

    private DataUsageRepository repository;

    public MainActivityViewModel(DataUsageRepository repository) {
        this.repository = repository;

        repository.initFetching(getCompositeDisposable());
    }

    public LiveData<PagedList<YearlyUsage>> getYearlyUsageData() {
        return repository.getYearlyUsageData();
    }

    public LiveData<ProgressStatus> getProgressStatus() {
        return repository.getProgressStatus();
    }
}
