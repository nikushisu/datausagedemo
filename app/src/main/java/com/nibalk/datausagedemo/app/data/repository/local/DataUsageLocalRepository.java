package com.nibalk.datausagedemo.app.data.repository.local;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.nibalk.datausagedemo.app.data.datasource.local.DataUsageLocalDataSourceFactory;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.NUMBERS_OF_THREADS;

public class DataUsageLocalRepository {

    final private static String TAG = "DUApp-" + DataUsageLocalRepository.class.getSimpleName();

    private LiveData<PagedList<YearlyUsage>> yearlyUsagePagedList;

    public DataUsageLocalRepository(DataUsageLocalDataSourceFactory dataSourceFactory) {
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(Integer.MAX_VALUE)
                .setPageSize(Integer.MAX_VALUE).build();

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder(
                dataSourceFactory, pagedListConfig);
        yearlyUsagePagedList = livePagedListBuilder.setFetchExecutor(executor).build();
    }

    public LiveData<PagedList<YearlyUsage>> getYearlyUsagePagedList() {
        return yearlyUsagePagedList;
    }
}
