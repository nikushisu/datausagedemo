package com.nibalk.datausagedemo.app.data.datasource.local;

import android.arch.paging.DataSource;

import com.nibalk.datausagedemo.app.data.database.YearlyUsageDao;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;
import com.nibalk.datausagedemo.app.data.repository.local.DataUsageLocalRepository;


public class DataUsageLocalDataSourceFactory extends DataSource.Factory<Integer, YearlyUsage> {

    final private static String TAG = "DUApp-" + DataUsageLocalRepository.class.getSimpleName();

    private DataUsageLocalPageKeyedDataSource localDataSource;


    public DataUsageLocalDataSourceFactory(YearlyUsageDao dao) {
        localDataSource = new DataUsageLocalPageKeyedDataSource(dao);
    }

    @Override
    public DataSource<Integer, YearlyUsage> create() {
        return localDataSource;
    }
}
