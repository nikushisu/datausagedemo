package com.nibalk.datausagedemo.app.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.text.TextUtils;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.DB_TABLE_NAME;

@Entity(tableName = DB_TABLE_NAME)
public class YearlyUsage {

    @PrimaryKey()
    @NonNull
    @ColumnInfo(name = "year")
    private String year;

    @ColumnInfo(name = "quarterVolumeList")
    private String quarterVolumeList;

    @ColumnInfo(name = "totalVolume")
    private String totalVolume;

    @ColumnInfo(name = "isShowImage")
    private boolean isShowImage;

    public YearlyUsage(String year, String quarterVolumeList, String totalVolume, boolean isShowImage) {
        this.year = year;
        this.quarterVolumeList = quarterVolumeList;
        this.totalVolume = totalVolume;
        this.isShowImage = isShowImage;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getQuarterVolumeList() {
        return quarterVolumeList;
    }

    public void setQuarterVolumeList(String quarterVolumeList) {
        this.quarterVolumeList = quarterVolumeList;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public boolean isShowImage() {
        return isShowImage;
    }

    public void setShowImage(boolean showImage) {
        isShowImage = showImage;
    }

    public static DiffUtil.ItemCallback<YearlyUsage> DIFF_CALLBACK = new DiffUtil.ItemCallback<YearlyUsage>() {
        @Override
        public boolean areItemsTheSame(@NonNull YearlyUsage oldItem, @NonNull YearlyUsage newItem) {
            return TextUtils.equals(oldItem.year, newItem.year);
        }

        @Override
        public boolean areContentsTheSame(@NonNull YearlyUsage oldItem, @NonNull YearlyUsage newItem) {
            return oldItem.equals(newItem);
        }
    };

    @Override
    public String toString() {
        return "YearlyUsage{" +
                "year='" + year + '\'' +
                ", quarterVolumeList='" + quarterVolumeList + '\'' +
                ", totalVolume='" + totalVolume + '\'' +
                ", isShowImage=" + isShowImage +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        YearlyUsage item = (YearlyUsage) obj;
        return TextUtils.equals(item.year, this.year);
    }
}
