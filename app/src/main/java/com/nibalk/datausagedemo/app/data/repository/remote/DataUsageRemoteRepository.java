package com.nibalk.datausagedemo.app.data.repository.remote;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.nibalk.datausagedemo.app.data.datasource.remote.DataUsageRemoteDataSourceFactory;
import com.nibalk.datausagedemo.app.data.datasource.remote.DataUsageRemotePageKeyedDataSource;
import com.nibalk.datausagedemo.app.data.model.ProgressStatus;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.NUMBERS_OF_THREADS;

public class DataUsageRemoteRepository {

    final private static String TAG = "DUApp-" + DataUsageRemoteRepository.class.getSimpleName();

    private LiveData<PagedList<YearlyUsage>> yearlyUsagePagedList;
    private LiveData<ProgressStatus> liveProgressStatus;

    public DataUsageRemoteRepository(DataUsageRemoteDataSourceFactory dataSourceFactory,
                                    PagedList.BoundaryCallback<YearlyUsage> boundaryCallback){

        PagedList.Config pagedListConfig =
                new PagedList.Config.Builder()
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(10)
                        .setPageSize(10).build();

        liveProgressStatus = Transformations.switchMap(dataSourceFactory.getRemoteDataSourceLive(),
                (Function<DataUsageRemotePageKeyedDataSource, LiveData<ProgressStatus>>)
                DataUsageRemotePageKeyedDataSource::getProgressLiveStatus);

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);
        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder<>(
                dataSourceFactory, pagedListConfig);
        yearlyUsagePagedList = livePagedListBuilder.
                setFetchExecutor(executor).
                setBoundaryCallback(boundaryCallback).
                build();

    }

    public LiveData<PagedList<YearlyUsage>> getYearlyUsagePagedList() {
        return yearlyUsagePagedList;
    }

    public LiveData<ProgressStatus> getLiveProgressStatus() {
        return liveProgressStatus;
    }
}
