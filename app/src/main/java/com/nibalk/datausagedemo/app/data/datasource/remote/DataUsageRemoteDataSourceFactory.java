package com.nibalk.datausagedemo.app.data.datasource.remote;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.nibalk.datausagedemo.app.data.model.YearlyUsage;
import com.nibalk.datausagedemo.app.data.service.DataUsageService;
import com.nibalk.framework.network.service.WebServiceManager;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.ReplaySubject;


public class DataUsageRemoteDataSourceFactory extends DataSource.Factory {


    private MutableLiveData<DataUsageRemotePageKeyedDataSource> remoteDataSourceLive;
    private DataUsageRemotePageKeyedDataSource remoteDataSource;

    public DataUsageRemoteDataSourceFactory(DataUsageService dataUsageService,
                                            WebServiceManager webServiceManager,
                                            CompositeDisposable compositeDisposable) {
        this.remoteDataSource = new DataUsageRemotePageKeyedDataSource(
                dataUsageService, webServiceManager, compositeDisposable);
        this.remoteDataSourceLive = new MutableLiveData<>();
    }

    @Override
    public DataSource create() {
        remoteDataSourceLive.postValue(remoteDataSource);
        return remoteDataSource;
    }

    public MutableLiveData<DataUsageRemotePageKeyedDataSource> getRemoteDataSourceLive() {
        return remoteDataSourceLive;
    }

    public ReplaySubject<YearlyUsage> getYearlyDataUsage() {
        return remoteDataSource.getYearlyDataUsage();
    }
}
