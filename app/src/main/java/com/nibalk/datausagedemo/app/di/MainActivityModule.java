package com.nibalk.datausagedemo.app.di;

import android.arch.lifecycle.ViewModelProvider;

import com.nibalk.datausagedemo.app.data.repository.DataUsageRepository;
import com.nibalk.framework.base.di.ViewModelProviderFactory;
import com.nibalk.datausagedemo.app.viewmodel.MainActivityViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    //-- ViewModel

    @Provides
    MainActivityViewModel mainActivityViewModel(DataUsageRepository dataUsageRepository) {
        return new MainActivityViewModel(dataUsageRepository);
    }

    @Provides
    ViewModelProvider.Factory provideMainActivityViewModel(MainActivityViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }

}
