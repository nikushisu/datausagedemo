package com.nibalk.datausagedemo.app.data.repository;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import com.nibalk.datausagedemo.app.data.model.ProgressStatus;
import com.nibalk.datausagedemo.app.data.model.YearlyUsage;

import io.reactivex.disposables.CompositeDisposable;

public interface DataUsageRepository {

    void initFetching(CompositeDisposable compositeDisposable);

    LiveData<PagedList<YearlyUsage>> getYearlyUsageData();

    LiveData<ProgressStatus> getProgressStatus();

}
