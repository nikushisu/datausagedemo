package com.nibalk.datausagedemo.app.data.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.nibalk.datausagedemo.app.data.model.YearlyUsage;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.DB_NAME;

@Database(entities = {YearlyUsage.class}, version = 1, exportSchema = false)
public abstract class YearlyUsageDB extends RoomDatabase {

    private static YearlyUsageDB instance;

    public abstract YearlyUsageDao getYearlyUsageDao();

    public static YearlyUsageDB getYearlyUsageDB(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    YearlyUsageDB.class, DB_NAME)
                    .build();
        }
        return instance;
    }
}
