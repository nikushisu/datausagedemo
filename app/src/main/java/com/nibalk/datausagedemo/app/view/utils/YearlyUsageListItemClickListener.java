package com.nibalk.datausagedemo.app.view.utils;

public interface YearlyUsageListItemClickListener {

    void onClicked(int position, boolean isImageClicked);

    void onLongClicked(int position);
}
