package com.nibalk.datausagedemo.app.data.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.nibalk.datausagedemo.app.data.model.YearlyUsage;

import java.util.List;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.DB_TABLE_NAME;

@Dao
public interface YearlyUsageDao {
    @Query("SELECT * FROM " + DB_TABLE_NAME)
    List<YearlyUsage> getYearlyUsage();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertYearlyUsage(YearlyUsage yearlyUsage);

    @Query("DELETE FROM " + DB_TABLE_NAME)
    abstract void deleteAllYearlyUsage();
}
