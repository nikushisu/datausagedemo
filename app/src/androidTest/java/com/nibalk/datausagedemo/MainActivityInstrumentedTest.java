package com.nibalk.datausagedemo;

import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.nibalk.datausagedemo.app.view.activity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.is;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void clickRowAtIndex0() {
        onClickRow(0);
    }

    @Test
    public void clickRowAtIndex3() {
        onClickRow(3);
    }

    @Test
    public void clickRowAtIndex6() {
        onClickRow(6);
    }

    private void onClickRow(int index) {
        onView(ViewMatchers.withId(R.id.list)).perform(
                RecyclerViewActions.scrollToPosition(index));

        onView(withId(R.id.list)).perform(
                RecyclerViewActions.actionOnItemAtPosition(index, ViewActions.click()));

        onView(withText("Clicked list row at " + (index + 1)))
                .inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }
}
