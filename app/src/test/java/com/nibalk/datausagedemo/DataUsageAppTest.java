package com.nibalk.datausagedemo;

import com.nibalk.datausagedemo.app.DataUsageApp;
import com.nibalk.datausagedemo.app.di.AppComponent;
import com.nibalk.datausagedemo.app.di.DaggerAppComponent;

public class DataUsageAppTest extends DataUsageApp {

    @Override
    public AppComponent getAppComponent() {

        return DaggerAppComponent.builder()
                .application(this)
                .build();
    }
}
