package com.nibalk.datausagedemo;

import android.app.Application;
import android.content.Context;

import com.nibalk.datausagedemo.app.di.module.AppModule;

import org.mockito.Mockito;

import javax.inject.Named;

import static com.nibalk.datausagedemo.app.utils.Constants.AppUrls.BASE_URL;
import static com.nibalk.framework.base.Constants.Keys.BASE_URL_KEY;

public class AppModuleTest extends AppModule {

    @Override
    public Context provideContext(Application application) {
        return Mockito.mock(Context.class);
    }

    @Override
    @Named(BASE_URL_KEY)
    public String provideBaseUrl() {
        return new String(BASE_URL);
    }
}
