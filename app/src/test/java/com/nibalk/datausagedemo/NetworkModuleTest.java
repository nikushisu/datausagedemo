package com.nibalk.datausagedemo;

import com.nibalk.datausagedemo.app.DataUsageApp;
import com.nibalk.framework.base.Constants;
import com.nibalk.framework.network.di.NetworkModule;

import org.mockito.Mockito;

import javax.inject.Named;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class NetworkModuleTest extends NetworkModule {

    @Override
    public HttpLoggingInterceptor provideHttpLoggingInterceptor(){
        return Mockito.mock(HttpLoggingInterceptor.class);
    }

    @Override
    public OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return Mockito.mock(OkHttpClient.class);
    }

    @Override
    public Retrofit provideRetrofit(OkHttpClient client, @Named(Constants.Keys.BASE_URL_KEY) String baseUrl) {
        return Mockito.mock(Retrofit.class);
    }

    public DataUsageApp provideApp() {
        return Mockito.mock(DataUsageApp.class);
    }
}
