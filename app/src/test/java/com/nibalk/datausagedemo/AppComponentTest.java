package com.nibalk.datausagedemo;

import com.nibalk.datausagedemo.app.di.AppComponent;
import com.nibalk.framework.network.di.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class, AppModuleTest.class})
public interface AppComponentTest extends AppComponent {
    void inject(DataUsageServiceTest test);
}