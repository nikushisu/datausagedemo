package com.nibalk.datausagedemo;

import com.nibalk.datausagedemo.app.DataUsageApp;
import com.nibalk.datausagedemo.app.data.model.DataUsage;
import com.nibalk.datausagedemo.app.data.service.DataUsageService;
import com.nibalk.datausagedemo.app.di.DaggerAppComponent;

import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;

import static com.nibalk.datausagedemo.app.utils.Constants.Values.QUERY_PARAMS_LIMIT;
import static com.nibalk.datausagedemo.app.utils.Constants.Values.QUERY_PARAMS_RESOURCE_ID;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class DataUsageServiceTest extends DataUsageAppTest {

    @Inject
    DataUsageService dataUsageService;


    @Before
    public void setUp() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);
    }

    @Test
    public void fetDataUsageInfo() {
        if (dataUsageService != null) {
            when(dataUsageService.fetDataUsageInfo(4, QUERY_PARAMS_LIMIT, QUERY_PARAMS_RESOURCE_ID))
                    .thenReturn(null);
        }
    }


}
