package com.nibalk.framework.network.service;

import android.net.ConnectivityManager;

import com.nibalk.framework.network.utils.NetworkUtils;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class WebServiceManagerImpl implements WebServiceManager {

    private ConnectivityManager connectivityManager;

    public WebServiceManagerImpl(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    @Override
    public <T> Observable<T> execute(final Call<T> call) {
        return Observable.just(NetworkUtils.isInternetAvailable(connectivityManager))
                .flatMap(available -> {
                    if (available) {
                        Timber.d("Network Available");
                        return executeServiceCall(call);
                    } else {
                        Timber.d("Network Not Available");
                        return Observable.error(new RuntimeException("Device is offline"));
                    }
                });
    }

    private <T> Observable<T> executeServiceCall(final Call<T> call) {
        Timber.d("executeServiceCall()");

        Observable<T> observable = Observable.fromCallable(() -> {
            try {
                Response<T> response = call.execute();

                if (response.isSuccessful()) {
                    Timber.d("Service call execution is successful");
                    return response.body();
                } else {
                    Timber.d("Service call execution is unsuccessful -> " + response.code());
                    return  null;
                }
            } catch (Exception e) {
                Timber.d("Service call execution is unsuccessful -> " + e.getMessage());
                throw e;
            }
        });

        return observable;
    }
}
