package com.nibalk.framework.network.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {


    public static boolean isInternetAvailable(ConnectivityManager connectivityManager) {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean isCurrentlyConnected = networkInfo != null && networkInfo.isConnected();

        return isCurrentlyConnected;
    }
}
