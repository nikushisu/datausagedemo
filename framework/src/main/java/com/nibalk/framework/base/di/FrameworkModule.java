package com.nibalk.framework.base.di;

import com.nibalk.framework.network.di.NetworkModule;

import dagger.Module;

@Module(includes = {NetworkModule.class})
public class FrameworkModule {


}
