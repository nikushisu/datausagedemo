package com.nibalk.framework.base.datasource;

import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

public abstract class BaseDataSource<T> extends PositionalDataSource<T> {
    abstract protected int countItems();
    abstract protected List<T> loadRangeAtPosition(int position, int size);

    @Override
    public void loadInitial(@NonNull LoadInitialParams params,
                            @NonNull LoadInitialCallback<T> callback) {
        int total=countItems();

        if (total==0) {
            callback.onResult(Collections.emptyList(), 0, 0);
        }
        else {
            final int position=computeInitialLoadPosition(params, total);
            final int size=computeInitialLoadSize(params, position, total);
            List<T> list=loadRangeAtPosition(position, size);

            if (list!=null && list.size()==size) {
                callback.onResult(list, position, total);
            } else {
                invalidate();
            }
        }
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params,
                          @NonNull LoadRangeCallback<T> callback) {
        List<T> list=loadRangeAtPosition(params.startPosition, params.loadSize);

        if (list!=null) {
            callback.onResult(list);
        } else {
            invalidate();
        }
    }
}