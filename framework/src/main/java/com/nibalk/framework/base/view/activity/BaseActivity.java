package com.nibalk.framework.base.view.activity;

import android.arch.lifecycle.ViewModel;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import dagger.android.AndroidInjection;
import timber.log.Timber;

public abstract class BaseActivity <VM extends ViewModel, VDB extends ViewDataBinding> extends AppCompatActivity {

    private VM viewModel;
    private VDB dataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Timber.d("onCreate()");

        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        this.viewModel = this.viewModel != null ? this.viewModel : setupViewModel();

        try {
            this.dataBinding = setupDataBinding();
        } catch (Exception e) {
            Timber.d("Data binding issue");
        }
    }

    protected abstract VM setupViewModel();
    protected abstract VDB setupDataBinding();

    protected VM getViewModel() {
        return viewModel;
    }
    protected VDB getDataBinding() {
        return dataBinding;
    }


}
